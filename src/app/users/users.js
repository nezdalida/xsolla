angular.module("xsolla.users", ['users.list', 'users.logs'])
        .config(function ($stateProvider) {
          // Настройки роутинга
          $stateProvider
                  .state('users', {
                    url: '/',
                    views: {
                      '@': {
                        templateUrl: '/app/users/index.html',
                      },
                      'content@users': {
                        templateUrl: '/app/list/list.html',
                        controller: listCtrl
                      }
                    }
                  })
        })
        .directive('ngAltElement', ngAltElement)

// Вывод альтернативных элементов (чекбокс блокировки, изменение баланса)
function ngAltElement($compile) {
  return {
    restrict: 'A',
    link: function (scope, element, attrs) {
      if (scope.col.element) {
        var tpl = '';
        switch (scope.col.element) {
          case 'checkbox':
            tpl = "<input type='checkbox' class='checkbox' id='checkbox_{{user.user_id}}' ng-model='user[col.name]' />"
                + "<label for='checkbox_{{user.user_id}}' ng-click='lockUser(user.user_id); $event.preventDefault(); $event.stopPropagation();'></label>";
            break;
          case 'link':
            tpl = "<a ng-click='editBalance(user.user_id); $event.stopPropagation();'>{{user[col.name]}}</a>"
            break;
        }
        var newElement = angular.element(tpl);
        $compile(newElement)(scope);
        element.append(newElement);
      }
    }
  }
}