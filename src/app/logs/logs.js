angular.module("users.logs", [])
        .config(function ($stateProvider) {
          // Route options
          $stateProvider
                  .state('users.logs', {
                    url: 'logs/:id',
                    views: {
                      'content@users': {
                        templateUrl: '/app/logs/logs.html',
                        controller: logsCtrl
                      }
                    }
                  });
        })
        .controller('logsCtrl', logsCtrl)

function logsCtrl($scope, $state, $filter, Restangular) {
  var id = $state.params.id;
  // @todo More filters
  $scope.request = {
    datetime_from: '2015-10-01T12:00:00+04:00',
    datetime_to: '2015-10-31T12:00:00+04:00'
  }
  $scope.logs = [];
  $scope.tablehead = [
    {name: "operation_id", title: "ID", show: true},
    {name: "transaction_id", title: "Transaction", show: true},
    {name: "external_id", title: "External", show: true},
    {name: "coupon_code", title: "Coupon", show: true},
    {name: "transaction_type", title: "Type", show: true},
    {name: "comment", title: "Comment", show: true},
    {name: "date", title: "Date", show: true, filterEx: "date"},
    {name: "amount", title: "Amount", show: true},
    {name: "currency", title: "Currency", show: true},
    {name: "project", title: "Project", show: true, real: "localized_name"},
    {name: "status", title: "Status", show: true},
    {name: "user_balance", title: "Balance", show: true},
    {name: "user_id", title: "UID", show: true},
    {name: "user_name", title: "Name", show: false},
    {name: "user_custom", title: "Custom", show: false},
    {name: "virtual_items", title: "Items", show: false},
  ]
  
  $scope.getLogs = function () {
    Restangular.one('users', id)
            .getList('transactions', $scope.request)
            .then(function (res) {
              $scope.logs = res;
            })
  }
  
  $scope.getLogs();

  $scope.filterFields = function (val, idx, data) {
    _.forEach($scope.tablehead, function (col) {
      if (col.filterEx) {
        switch (col.filterEx) {
          case 'date':
            val[col.name] = $filter('date')(val[col.name], 'medium');
            break;
        }
      }
    })
    return val;
  }
}
