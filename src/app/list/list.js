angular.module("users.list", [])
        .config(function ($stateProvider) {
          // Route options
          $stateProvider
                  .state('users.list', {
                    url: 'list',
                    views: {
                      'content@users': {
                        templateUrl: '/app/list/list.html',
                        controller: listCtrl
                      }
                    }
                  });
        })
        .controller('listCtrl', listCtrl)
        .controller('editCtrl', editCtrl)
        .factory('Users', function (Restangular) {
          return Restangular.all('users');
        });

function listCtrl($scope, $filter, $uibModal, $state, Users, toaster, Restangular) {
  var offset = 0;
  var limit = 10;
  $scope.total = 0;
  // Table header & options
  $scope.tablehead = [
    {name: "user_id", title: "ID"},
    {name: "user_name", title: "Username"},
    {name: "user_custom", title: "Custom"},
    {name: "email", title: "Email"},
    {name: "register_date", title: "Register Date"},
    {name: "balance", title: "Balance", element: 'link', style: "width: 150px"},
    {name: "enabled", title: "Enabled", element: 'checkbox', style: "width:70px;"}
  ];

  // Get data from server
  Users.get('', {offset: offset, limit: limit}).then(function (res) {
    _.forEach(res.data, function (el) {
      el.register_date = $filter('date')(el.register_date, 'medium');
    })
    $scope.total = res.recordsTotal;
    $scope.users = res.data;
  });

  // Table sorting
  $scope.predicate = 'user_id';
  $scope.sortTable = function (column) {
    if ($scope.predicate !== column) {
      $scope.predicate = column;
      $scope.reverse = true;
    } else {
      $scope.reverse = !$scope.reverse;
    }
  };
  
  $scope.editBalance = function (user) {
    user.editable = !user.editable
    if(!user.editable) {
      Restangular.one('users', user.user_id)
          .post('recharge', {amount: user.balance, comment: 'Changed by Admin'})
          .then(function (res) {
            toaster.pop('success', 'Success!', 'Balance was updated');
          })
    }
  }

  $scope.lockUser = function (user) {
    Users.customPUT({enabled: !user.enabled}, user.user_id).then(function(res){
      // Hello? Is anyone reply?
      var locked = (user.enabled) ? 'unlocked' : 'locked';
      toaster.pop('info', 'Info', 'User ' + locked);
    })
  }
  
  $scope.createUser = function () {
    var options = {
      animation: true,
      templateUrl: '/app/edit/edit.html',
      controller: 'editCtrl',
      resolve: {
        user: function() {
          return {
            user_id: '',
            user_name: '',
            user_custom: '',
            email: '',
            enabled: true
          }
        }
      }
    };
    $scope.editor(options);
  }

  $scope.editUser = function (user) {
    var options = {
      animation: true,
      templateUrl: '/app/edit/edit.html',
      controller: 'editCtrl',
      resolve: {
          user: function() {
            return {
              user_id: user.user_id,
              user_name: user.user_name,
              user_custom: user.user_custom,
              email: user.email,
              enabled: user.enabled
            };
          }
        }
    };
    $scope.editor(options);
  }

  // Modal
  $scope.editor = function (options) {
    var modalInstance = $uibModal.open(options);
    modalInstance.result.then(function (item) {
      $state.go('users.list', {}, {reload: true});
    });
  };
}

// Editor
function editCtrl($scope, $modalInstance, user, Users, toaster) {
  $scope.newUser = (user.user_id == '') ? true : false;
  $scope.user = user;
  $scope.label = ($scope.user.user_id) ? 'Edit' : 'Create';
  $scope.status = ($scope.user.enabled) ? 'enabled' : 'disabled';
  
  $scope.toggleStatus = function() {
    $scope.status = (!$scope.user.enabled) ? 'enabled' : 'disabled';
  }
  
  // Send data
  $scope.ok = function () {
    if($scope.newUser) {
      Users.post(user).then(function() {
        toaster.pop('success', 'Success', 'User created')
        $modalInstance.close($scope.user);
      })
    } else {
      //Restangular.restangularizeElement(null, user, 'users');
      // Cause 'This form should not contain extra fields: "id"'
      Users.customPUT(user, user.user_id).then(function(res){
        // Hello? Is anyone reply?
        toaster.pop('success', 'Success', 'User updated')
        $modalInstance.close($scope.user);
      })
    }
  };

  // Cancel
  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
}