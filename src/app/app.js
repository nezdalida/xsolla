;
(function () {
  "use strict";
  angular.module("xsolla", ['ui.router', 'restangular', 'ngStorage', 'toaster', 'ui.bootstrap', 'datePicker', 'xsolla.users'])
          .run(function ($rootScope, $state, $log, $sessionStorage, Restangular, toaster) {
            $rootScope.$state = $state;
            $rootScope.$storage = $sessionStorage.$default({
              token: ''
            });
            Restangular.addResponseInterceptor(function (data, operation, what, url, response, deferred) {
              if (data.http_status_code) {
                $log.warn('Error ' + data.http_status_code, data.message);
                if (data.extended_message) {
                  _.forEach(data.extended_message.global_errors, function (text, key) {
                    toaster.pop('error', 'Global Error', text);
                  });
                  _.forEach(data.extended_message.property_errors, function (text, key) {
                    toaster.pop('warning', key, text[0]);
                  });
                }
              }
              return data;
            })
            Restangular.addErrorInterceptor(function (response, deferred, responseHandler) {
              var errors = [403, 404, 500]; // основные ошибки заглушим
              var handle = true;
              _.forEach(errors, function (el) {
                if (response.status == el) {
                  toaster.pop('error', 'Ooops!', "Something has gone wrong");
                  handle = false;
                  return false; // exit from foreach
                }
              })
              return handle; // error not handled
            })
          })
          .config(function ($locationProvider, RestangularProvider) {
            RestangularProvider.setBaseUrl('https://livedemo.xsolla.com/fe/test-task/korobeinikov/');
            $locationProvider.html5Mode(true).hashPrefix('!');
          })
          .controller()
})();
